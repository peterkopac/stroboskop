git commit -a -m "Priprava potrebnih JavaScript knjižnjic"git commit -a -m "Priprava potrebnih JavaScript knjižnjic"# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

git clone https://peterkopac@bitbucket.org/peterkopac/stroboskop.git

Naloga 6.2.3:
https://bitbucket.org/peterkopac/stroboskop/commits/98990dcf460dd6ece33a019ac9b43a223ffb301d


## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/peterkopac/stroboskop/commits/ac8b83d3daf02a7200660b3265f00dad440a15e9

Naloga 6.3.2:
https://bitbucket.org/peterkopac/stroboskop/commits/c29a1c302723695e23ab8dc8628b0ea62f9dcb7b

Naloga 6.3.3:
https://bitbucket.org/peterkopac/stroboskop/commits/eee13ee6387ebda9e555a697d538f8f940e9e0e8

Naloga 6.3.4:
https://bitbucket.org/peterkopac/stroboskop/commits/3b97746314c5a59cff8b570f5d4e7f0bd5c15f84

Naloga 6.3.5:

git checkout master 
git merge master izgled 
git push -u origin master

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/peterkopac/stroboskop/commits/272e737533f7f9a1dfee9d50b2813e93c18ce572

Naloga 6.4.2:
https://bitbucket.org/peterkopac/stroboskop/commits/805769613f12c4d6366170cfc413315c5b681600

Naloga 6.4.3:
https://bitbucket.org/peterkopac/stroboskop/commits/fcb7c7f32f4c4b5363feb078e65720c4d74f7dd5

Naloga 6.4.4:
https://bitbucket.org/peterkopac/stroboskop/commits/9cbed99bfb46e342bc2666e298e2c12d9327baaa